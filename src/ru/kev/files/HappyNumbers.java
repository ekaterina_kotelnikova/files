package ru.kev.files;

import java.io.*;

/**
 * Класс для нахождения счастливых чисел и записи их в файл "txtdata.dat"
 *
 * @author Kotelnikova E.V. group 15oit20
 */
public class HappyNumbers {

    public static void main(String[] args) {
        try (DataInputStream reader = new DataInputStream(new FileInputStream("int6data.dat"))) {
            try (DataOutputStream writer = new DataOutputStream(new FileOutputStream("txtdata.dat"))) {

                int number;
                while (reader.available() != 0) {
                    number = reader.readInt();
                    int tmp = number;
                    int[] masOfNumber = new int[6];
                    for (int i = 5; i >= 0; i--) {
                        masOfNumber[i] = tmp % 10;
                        tmp = tmp / 10;
                    }

                    int part1 = masOfNumber[0] + masOfNumber[1] + masOfNumber[2];
                    int part2 = masOfNumber[3] + masOfNumber[4] + masOfNumber[5];
                    if (part1 == part2) {
                        writer.writeInt(number);
                        System.out.println(number + " - счастливое число");
                    }
                }
            } catch (IOException e) {
                System.err.println("Ошибка ввода-ввывода данных");
            }

        } catch (FileNotFoundException e) {
            System.err.println("Файл не найден");
        } catch (IOException e) {
            System.err.println("Ошибка ввода-ввывода данных");
        }
    }
}

