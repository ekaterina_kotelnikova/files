package ru.kev.files;

import java.io.*;

/**
 * Класс для для заполнения файла "intdata.dat" данными из текстового файла "numbers.txt'
 *
 * @author Kotelnikova E.V. group 15oit20
 */

public class IntegerNumbers {

    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new FileReader("numbers.txt"))) {

            try (DataOutputStream writer = new DataOutputStream(new FileOutputStream("intdata.dat"))) {
                String numbers;
                while ((numbers = reader.readLine()) != null) {
                    writer.writeInt(Integer.valueOf(numbers));
                }
            } catch (IOException e) {
                System.err.println("Ошибка ввода-ввывода данных");
            } catch (NumberFormatException e) {
                System.err.println("Неверный формат данных");
            }

        } catch (FileNotFoundException e) {
            System.err.println("Файл не найден");
        } catch (IOException e) {
            System.err.println("Ошибка ввода-ввывода данных");
        }

        System.out.println("Файл заполнен");
    }
}
