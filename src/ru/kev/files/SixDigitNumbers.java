package ru.kev.files;

import java.io.*;

/**
 * Класс для заполнения файла "int6data.dat" четырёхзначными  - шестизначными числами и вывода шестизначных чисел на экран
 *
 * @author Kotelnikova E.V. group 15oit20
 */

public class SixDigitNumbers {

    public static void main(String[] args) {
        try (DataInputStream reader = new DataInputStream(new FileInputStream("intdata.dat"))) {
            try (DataOutputStream writer = new DataOutputStream(new FileOutputStream("int6data.dat"))) {

                int number;
                while (reader.available() != 0) {
                    number = reader.readInt();
                    if (number > 999 && number < 1000000) {
                        writer.writeInt(number);
                        System.out.println(number + " - четырёхзначное - шестизначное число");
                    }
                }

            } catch (IOException e) {
                System.err.println("Ошибка ввода-ввывода данных");
            }

        } catch (FileNotFoundException e) {
            System.err.println("Файл не найден");
        } catch (IOException e) {
            System.err.println("Ошибка ввода-ввывода данных");
        }

    }
}

